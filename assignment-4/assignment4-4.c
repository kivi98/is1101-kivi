#include<stdio.h>
int main ()
{
	int num = 0, dev = 1;
	printf("Enter a number =");
	scanf("%d", &num);
	printf("\nFactors of '%d' are;\n", num);
	do
	{
		if (num % dev == 0)
			printf(">>> %d\n", dev);
		dev = dev+1;
	}
	while (dev <= num);
	return 0;
}
