#include<stdio.h>

int main ()
{
	int n=0, y=1, x=0, z=1, p=1;
	printf("How many multiplication tables you need? > ");
	scanf("%d", &n);
	do
	{
		printf("\n\nMultiplication table number %d\n" ,p);
		y=1;
		do
		{
			x = z*y;
			printf(" %d * %d = %d\n", z,y,x);
			y=y+1;
		}
		while (y<=10);
		z=z+1;
		n=n-1;
		p=p+1;
	}
	while (n>=1);
	return 0;
}
