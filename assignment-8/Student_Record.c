#include<stdio.h>
struct std_rec{
	char name[20];
	char sub[20];
	int marks;
};
int main(){
	struct std_rec array[5];
	for (int i=0;i<5;i++){
		printf("Enter the details of student number %d\n", i+1);

		printf("First name of the student > ");
		scanf("%s", array[i].name);

		printf("Enter the subject > ");
		scanf("%s", array[i].sub);

		printf("Enter marks > ");
		scanf("%f", array[i].marks);
		printf("\n");
	}
	printf("Name\t\tSubject\t\tMarks\n");
	printf("_________________________________________\n");
	for (int i=0;i<5;i++){
		printf("%s\t\t%s\t\t%d\n", array[i].name, array[i].sub, array[i].marks);
	}
	return 0;
}
