#include<stdio.h>
int main ()
{
	char vowel;
	printf("Enter a character =");
	scanf("%c", &vowel);
	if (vowel=='a' || vowel=='e' || vowel=='i' || vowel=='o' || vowel=='u')
		printf("'%c' is a Vowel\n", vowel);
	else if (vowel=='A' || vowel=='E' || vowel=='I' || vowel=='O' || vowel=='U')
	       printf("'%c' is a Vowel\n", vowel);
	else
	       printf("'%c' is a Consonant\n", vowel);
	return 0;	

}
