#include<stdio.h>
int main(){
	char sen[500], reversed[500];
	int start, last, count =0;

	printf("Enter the sentence you want to reverse > ");
	scanf("%[^\n]", &sen);

	for (;sen[count]!='\0';count++)
		last=count;
	for (start=0; start<count;start++){
		reversed[start] = sen[last];
		last--;
	}
	printf("The reversed sentence is > %s\n\n", reversed);
	return 0;
}
