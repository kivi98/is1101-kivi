#include<stdio.h>
#define MAX 500
int main(){
	char sen[MAX]; 
	char n;
	int i, count=0;
	printf("\nEnter your sentence to find the frequency > ");
	scanf("%[^\n]", &sen);
	printf("\nEnter character you need to check > ");
	scanf(" %c", &n);
	for (i=0;sen[i]!='\0';i++){
		if(n==sen[i])
			count++;
	}
	printf("Frequency of character %c is %d\n\n", n, count);
	return 0;
}
