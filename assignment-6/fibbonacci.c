#include<stdio.h>
int fibonacci(int n){
	if (n==0 || n==1)
		return n;
	else 
		return (fibonacci(n-1)+fibonacci(n-2));
}
int seq(int s){
	if (s>0){
		printf(" > %d\n", seq(s-1));
		fibonacci(s);
	}
	else 
		return 0;
}
int main () {
	int n;
	printf("\nEnter any number you want > ");
	scanf("%d", &n);
	seq(n);
	return 0;
}
